<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
	protected $fillable = array('fullname', 'username', 'email', 'password', 'bio', 'dateofbirth');

	public $datatypes = array (
		'fullname' => array('text', 50),
		'username' => array('text', 50),
		'email' => array('email', 50),
		'dateofbirth' => array('date', 50),
		'password' => array('password', 50),
		'bio' => array('text', 500)
	);

}
