<?php namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;



class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['users'] = User::all()->toArray();
		return view('user.dashboard', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{		
		return view('user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * @param Request $request Validates forms
	 * @return Response
	 */
	public function store(Request $request)
	{		
		User::create($request->all());
		return redirect('user')
				->with('message', 'New user created successfully'); 
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['user'] = User::findOrFail($id);
		return view('user.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		return view('User.edit')
					->with( ['user' => User::findOrFail($id)] );
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$t = User::findOrFail($id);

		$t->update($request->all());
		return redirect('user')
				->with('message', 'user updated successfully'); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$t = User::findOrFail($id);
		$t->delete();
		return redirect('user')
				->with('message', 'user deleted successfully'); 
	}

}
