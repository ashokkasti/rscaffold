<?php namespace App\Http\Controllers;

use Auth;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;



class ProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['products'] = Product::all()->toArray();
		return view('product.dashboard', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{		
		return view('product.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * @param Request $request Validates forms
	 * @return Response
	 */
	public function store(Request $request)
	{		
		Product::create($request->all());
		return redirect('product')
				->with('message', 'New product created successfully'); 
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['product'] = Product::findOrFail($id);
		return view('product.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		return view('Product.edit')
					->with( ['product' => Product::findOrFail($id)] );
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$t = Product::findOrFail($id);

		$t->update($request->all());
		return redirect('product')
				->with('message', 'product updated successfully'); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$t = Product::findOrFail($id);
		$t->delete();
		return redirect('product')
				->with('message', 'product deleted successfully'); 
	}

}
