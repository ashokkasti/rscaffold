<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return '<pre>


		Laravel Scaffold Boiler Plate - Ashok
			- Create CRUD operations [Bootstrap 3 support] with single command


		1. Create Model and Migration, migrate database schema

		2. In model create $datatypes array as follows
			<b>Syntax: </b>
			public $datatypes = array(
					"columnname" => array("datatype", maxlength, "relationshipModel")
				)

			<b>Example: </b>
			public $datatypes = array(
				"title" => array("text", 50),
				"addedby" => array("number", 10, "User")

				)

		3. Call <b>ashok:scaffold</b> command
		
			<b>Syntax: </b>
			php artisan ashok:scaffold ModelName

			<b>Example</b>
			php artisan ashok:scaffold Post


	</pre>';
});

Route::resource('user', 'UserController');