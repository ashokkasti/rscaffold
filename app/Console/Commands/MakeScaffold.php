<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Composer;

class MakeScaffold extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ashok:scaffold {model}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This will create a Bootstrap 3 Scaffold for CRUD operations automatically';

	/**
	 * This stores the file
	 * @var string
	 */
	protected $files;

	private $composer;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     * @param NameParser $parser
     * @param Composer $composer
     */
    public function __construct(Filesystem $files, Composer $composer)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = $composer;
    }

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $m = "App\\" . $this->argument('model');
        $m = new $m;
        if ( sizeof($m->datatypes) <= 0 ) {
            $this->error('Your scaffold couldnot be created. Please create a public $datatypes array');
            return false;
        }

        $this->makeController();
		$this->makeViews($m->datatypes);
		$this->info("Your scaffold for '" . $this->argument('model') . "' has been created successfully!!");
         $this->info("
                    ******* Please create the following routes ***********

                    Route::resource('" . camel_case($this->argument('model')) . "', '" . $this->argument('model') . "Controller');

                    *************** Thank you  - ashok *******************");
	}

	/**
	 * Generate controller for the desired Model
	 */
	protected function makeController () {
		$name = $this->argument('model');
        if ($this->files->exists($path = $this->getControllerPath($name))) {
            if ($this->ask($name . "Controller already exists. Do you want to replace (y/n) ?") != 'y') {
                 $this->error($name . "Controller not created as it already exists");
                 return false;
            }            
        }
        $this->files->put($path, $this->compileControllerStub());
        $this->info($name . 'Controller created');

	}


	/**
     * Get the path to where we should store the controller.
     *
     * @param  string $name
     * @return string
     */
    protected function getControllerPath($name)
    {
        return 'app/Http/Controllers/' . $name . 'Controller.php';
    }

     /**
     * Compile the controller stub.
     *
     * @return string
     */
    protected function compileControllerStub()
    {
        $stub = $this->files->get(__DIR__ . '/stubs/controller.stub');

        $this->replaceClassName($stub)
            ->replaceViewFolder($stub)
            ->replaceModel($stub);

        return $stub;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceClassName(&$stub)
    {
        $className = ucwords(camel_case($this->argument('model'))) . 'Controller';
        $stub = str_replace('{{class}}', $className, $stub);
        return $this;
    }

    /**
     * Replace the viewfolder in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceViewFolder(&$stub)
    {
        $viewFolder = strtolower($this->argument('model'));
        $stub = str_replace('{{viewfolder}}', $viewFolder, $stub);
        return $this;
    }

    /**
     * Replace the model in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceModel(&$stub)
    {
        $model = ucwords(camel_case($this->argument('model')));
        $stub = str_replace('{{model}}', $model, $stub);
        return $this;
    }


    /**
	 * Generate views for the desired Model
	 */
	protected function makeViews ($datatypes) {
		$name = $this->argument('model');
        if ($this->files->exists($path = $this->getViewPath('create'))) {
            if ($this->ask($name . '/create view file already exists, do you want to replace (y/n)?')  != 'y') {
                $this->error($name . '/create view not created as it already exists');
                return false;
            }
        }
        $this->makeDirectory($path);
        $this->files->put($path, $this->compileViewStub());
        $this->info($name . 'Views created');


        // Edit
        if ($this->files->exists($path = $this->getViewPath('edit'))) {
            if ($this->ask($name . '/edit view file already exists, do you want to replace (y/n)?')  != 'y') {
                $this->error($name . '/edit view not created as it already exists');
                return false;
            }
        }
        $this->makeDirectory($path);
        $this->files->put($path, $this->compileEditViewStub());
        $this->info($name . ' [Edit] Views created');


        // Dashboard
        if ($this->files->exists($path = $this->getViewPath('dashboard'))) {
            if ($this->ask($name . '/dashboard view file already exists, do you want to replace (y/n)?')  != 'y') {
                $this->error($name . '/dashboard view not created as it already exists');
                return false;
            }
        }
        $this->makeDirectory($path);
        $this->files->put($path, $this->compileDashboardViewStub());
        $this->info($name . ' [Dashboard] Views created');


        // Form
        if ($this->files->exists($path = $this->getViewPath('form'))) {
            if ($this->ask($name . '/form view file already exists, do you want to replace (y/n)?') != 'y') {
                $this->error($name . '/form view not created as it already exists');
                return false;
            }
        }

        $this->makeDirectory($path);
        $this->files->put($path, $this->compileFormViewStub($this->makeForm($datatypes)));
        $this->info($name . 'Views [Form] created');




	}

    /**
     * Create HTML form 
     * @param  Array $datatypes Array of Datas and its datatypes
     * @return string            HTML elements in form of strong
     */
    protected function makeForm ($datatypes) {
        $returnVar = "";
        foreach ($datatypes as $key => $datatype) {
            $returnVar .= '<div class="form-group">
            ';
                $returnVar .= '<label for="' . $key . '">' . camel_case($key) . ': </label>';
                
                if (isset($datatype[2])) {
                   $returnVar .= '
                        <?php
                        $fModel = App\\' . $datatype[2] .  '::all()->toArray();
                        $optionArr = [];
                        

                        foreach ($fModel as $child) {
                            $k = array_keys($child);
                            $optionArr[$child[$k[0]]] = $child[$k[1]];
                        }

                        ?>
                        
                        {!! Form::select("' . $key . '", $optionArr) !!}';

                } else if ($datatype[1] < 100) {
                    $returnVar .= "{!! Form::" . $datatype[0] . "('" . $key . "', null, array('class' => 'form-control', 'maxlength' => '" . $datatype[1] . "')) !!}";                   
                } else {
                    $returnVar .= "{!! Form::textarea('" . $key . "', null, array('class' => 'form-control', 'maxlength' => '" . $datatype[1] . "')) !!}";
                }

            $returnVar .= "
            </div>";
        }
        return $returnVar;
    }




	/**
     * Get the path to where we should store the views.
     *
     * @param  string $name
     * @return string
     */
    protected function getViewPath($name)
    {
        return 'resources/views/' . strtolower($this->argument('model')) . '/' .  $name . '.blade.php';
    }

    /**
     * Build the directory for the view if necessary.
     *
     * @param  string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }
    }


    /**
     * Compile the view stub.
     *
     * @return string
     */
    protected function compileViewStub()
    {

        $stub = $this->files->get(__DIR__ . '/stubs/create_view.stub');
         $stub = str_replace('{{model}}', camel_case($this->argument('model')), $stub);
        return $stub;
    }

    /**
     * Compile the view stub.
     *
     * @return string
     */
    protected function compileEditViewStub()
    {
        $stub = $this->files->get(__DIR__ . '/stubs/edit_view.stub');
        $stub = str_replace('{{model}}', camel_case($this->argument('model')), $stub);
        return $stub;
    }

    /**
     * Compile the Dashboard view stub.
     *
     * @return string
     */
    protected function compileDashboardViewStub()
    {
        $stub = $this->files->get(__DIR__ . '/stubs/dashboard_view.stub');
        $stub = str_replace('{{model}}', camel_case($this->argument('model')), $stub);
        $stub = str_replace('{{imodel}}', $this->argument('model'), $stub);
        return $stub;
    }

    /**
     * Compile the view - form stub.
     *
     * @return string
     */
    protected function compileFormViewStub($html)
    {
        $stub = $this->files->get(__DIR__ . '/stubs/form_view.stub');
        $stub = str_replace('{{htmlform}}', $html, $stub);
        $stub = str_replace('{{model}}', camel_case($this->argument('model')), $stub);
        return $stub;
    }


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['model', InputArgument::REQUIRED, 'User'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
