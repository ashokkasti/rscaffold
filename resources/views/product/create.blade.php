@extends('app')

@section('content')
	<h2>Create product</h2>
	{!! Form::open(array('url' => 'product')) !!}
		@include('product/form')
	{!! Form::close() !!}
@endsection