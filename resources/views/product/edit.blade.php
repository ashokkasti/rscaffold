@extends('app')

@section('content')
	<h2>Edit product</h2>
	{!! Form::model($product, array('url' => 'product/' . $product->id, 'method' => 'PUT' ) ) !!}
		@include('product/form')
	{!! Form::close() !!}
@endsection