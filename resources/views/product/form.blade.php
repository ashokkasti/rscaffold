
	<div class="form-group">
            <label for="productname">productname: </label>{!! Form::text('productname', null, array('class' => 'form-control', 'maxlength' => '50')) !!}
            </div><div class="form-group">
            <label for="price">price: </label>{!! Form::number('price', null, array('class' => 'form-control', 'maxlength' => '10')) !!}
            </div><div class="form-group">
            <label for="description">description: </label>{!! Form::textarea('description', null, array('class' => 'form-control', 'maxlength' => '500')) !!}
            </div><div class="form-group">
            <label for="addedby">addedby: </label>
                        <?php
                        $fModel = App\User::all()->toArray();
                        $optionArr = [];
                        

                        foreach ($fModel as $child) {
                            $k = array_keys($child);
                            $optionArr[$child[$k[0]]] = $child[$k[1]];
                        }

                        ?>
                        
                        {!! Form::select("addedby", $optionArr) !!}
            </div>
	<input type="submit" value="Save">