@extends('app')

@section('content')
	<h2>Product Dashboard</h2>
	<a href="{{url('product/create')}}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i></a>
	<table class="table table-striped">
		<thead>
				<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Action</th>
			</tr>
		</thead>
		@foreach($products as $product)
			<tr>
				<?php $keys = array_keys($product); ?>
				<td>{{ $product[$keys[0]] }}</td>
				<td>{{ $product[$keys[1]] }}</td>
				<td>
					{!! Form::open(array('url' => 'product/'. $product[$keys[0]] , 'method' => 'DELETE')) !!}
						<a href="{{ url('product/' . $product["id"] . '/edit') }}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i></a>
						{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', array('type=' => 'submit', 'class' => 'btn btn-sm btn-danger deletebtn', false) ) !!}
					{!! Form::close() !!}

				</td>
			</tr>
		@endforeach
	</table>
	
@endsection