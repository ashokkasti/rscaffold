
	<div class="form-group">
            <label for="fullname">fullname: </label>{!! Form::text('fullname', null, array('class' => 'form-control', 'maxlength' => '50')) !!}
            </div><div class="form-group">
            <label for="username">username: </label>{!! Form::text('username', null, array('class' => 'form-control', 'maxlength' => '50')) !!}
            </div><div class="form-group">
            <label for="email">email: </label>{!! Form::email('email', null, array('class' => 'form-control', 'maxlength' => '50')) !!}
            </div><div class="form-group">
            <label for="dateofbirth">dateofbirth: </label>{!! Form::date('dateofbirth', null, array('class' => 'form-control', 'maxlength' => '50')) !!}
            </div><div class="form-group">
            <label for="password">password: </label>{!! Form::password('password', null, array('class' => 'form-control', 'maxlength' => '50')) !!}
            </div><div class="form-group">
            <label for="bio">bio: </label>{!! Form::textarea('bio', null, array('class' => 'form-control', 'maxlength' => '500')) !!}
            </div>
	<input type="submit" value="Save">