@extends('app')

@section('content')
	<h2>User Dashboard</h2>
	<a href="{{url('user/create')}}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i></a>
	<table class="table table-striped">
		<thead>
				<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Action</th>
			</tr>
		</thead>
		@foreach($users as $user)
			<tr>
				<?php $keys = array_keys($user); ?>
				<td>{{ $user[$keys[0]] }}</td>
				<td>{{ $user[$keys[1]] }}</td>
				<td>
					{!! Form::open(array('url' => 'user/'. $user[$keys[0]] , 'method' => 'DELETE')) !!}
						<a href="{{ url('user/' . $user["id"] . '/edit') }}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i></a>
						{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', array('type=' => 'submit', 'class' => 'btn btn-sm btn-danger deletebtn', false) ) !!}
					{!! Form::close() !!}

				</td>
			</tr>
		@endforeach
	</table>
	
@endsection