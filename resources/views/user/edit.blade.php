@extends('app')

@section('content')
	<h2>Edit user</h2>
	{!! Form::model($user, array('url' => 'user/' . $user->id, 'method' => 'PUT' ) ) !!}
		@include('user/form')
	{!! Form::close() !!}
@endsection