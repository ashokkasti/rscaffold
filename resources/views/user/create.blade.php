@extends('app')

@section('content')
	<h2>Create user</h2>
	{!! Form::open(array('url' => 'user')) !!}
		@include('user/form')
	{!! Form::close() !!}
@endsection