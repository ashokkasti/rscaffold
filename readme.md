## rScaffold


Laravel Scaffold Boiler Plate - Ashok
- *Create CRUD operations [Bootstrap 3 support] with single command*

		1. Create Model and Migration, migrate database schema

		2. In model create $datatypes array as follows
			Syntax: 
			public $datatypes = array(
					"columnname" => array("datatype", maxlength, "relationshipModel")
				)

			Example: 
			public $datatypes = array(
				"title" => array("text", 50),
				"addedby" => array("number", 10, "User") // Relationship with another model

				)

		3. Call ashok:scaffold command
		
			Syntax: 
			php artisan ashok:scaffold ModelName

			Example
			php artisan ashok:scaffold Post
